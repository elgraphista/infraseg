<?php
if($_POST) {



    $to_Email = "m.alvarez.l@gmail.com"; // Write your email here
   
    // Use PHP To Detect An Ajax Request
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
   
        // Exit script for the JSON data
        $output = json_encode(
        array(
            'type'=> 'error',
            'text' => 'Request must come from Ajax'
        ));
       
        die($output);
    }
   
    // Checking if the $_POST vars well provided, Exit if there is one missing
    if(!isset($_POST["userName"]) || !isset($_POST["userEmail"]) || !isset($_POST["userContact"]) || !isset($_POST["userCompany"]) || !isset($_POST["userOption"])) {
        
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Campos Vacios!'));
        die($output);
    }
   
    // PHP validation for the fields required
    if(empty($_POST["userName"])) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Favor ingresar Nombre'));
        die($output);
    }
    
	if(empty($_POST["userContact"])) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Lo sentimos, pero no hay contacto.'));
        die($output);
    }
	
	if(empty($_POST["userCompany"])) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Ingrese su empresa'));
        die($output);
    }
	
	if(empty($_POST["userOption"])) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Ingrese el asunto'));
        die($output);
    }
	
    if(!filter_var($_POST["userEmail"], FILTER_VALIDATE_EMAIL)) {
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Ingrese un email valido.'));
        die($output);
    }

     
    // Proceed with PHP email
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
    $headers .= 'From: My website' . "\r\n";
    $headers .= 'Reply-To: '.$_POST["userEmail"]."\r\n";
    
    'X-Mailer: PHP/' . phpversion();
    
    // Body of the Email received in your Mailbox
    $emailcontent = 'Has recibido un nuevo mensaje del visitante <strong>'.$_POST["userName"].'</strong><br/><br/>'. "\r\n" .
                'Asunto : <br/> <em>'.$_POST["userOption"].'</em><br/><br/>'. "\r\n" .
				'Telefono : <br/> <em>'.$_POST["userContact"].'</em><br/><br/>'. "\r\n" .
				'Empresa : <br/> <em>'.$_POST["userCompany"].'</em><br/><br/>'. "\r\n" .
                '<strong>Siéntase libre de contactar a '.$_POST["userName"].' via email: '.$_POST["userEmail"].'</strong>' . "\r\n" ;
    
    $Mailsending = @mail($to_Email, "Nuevo correo desde la web" ,$emailcontent, $headers);
   
    if(!$Mailsending) {
        
        //If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)
        $output = json_encode(array('type'=>'error', 'text' => '<i class="icon ion-close-round"></i> Oops! Parece que algo salió mal, compruebe su configuración de correo PHP.'));
        die($output);
        
    } else {
        $output = json_encode(array('type'=>'message', 'text' => '<i class="icon ion-checkmark-round"></i> Hola '.$_POST["userName"] .', Su mensaje ha sido enviado, ¡nos comunicaremos con usted lo antes posible!'));
        die($output);
    }
}
?>
