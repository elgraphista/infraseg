"use strict";
var $ = jQuery.noConflict();
$(window).on("load", function () {
    $(".pre-loader").delay(1e3).fadeOut("slow"), $(".pre-loader").addClass("pre-out"), $(".pre-loader-anim").addClass("pre-fade"), $(".op-img").css("opacity", "0"), $(".clients_block").mouseleave(function () {
        $(this).removeClass("hover")
    }), $(".clients_block").mouseenter(function () {
        $(this).addClass("hover")
    })
}), $(document).on("ready", function () {
    var t = $(window).height();
    $(".full_banner").css("height", t), $(".la-anim-1").addClass("la-animate"), new WOW({
        boxClass: "wow",
        animateClass: "animated",
        offset: 0,
        mobile: !1,
        live: !0
    }).init(), $(".portfolio-categories").on("click", "li", function () {
        var t = $(this).attr("data-filter");
        $("#work-masonry").isotope({filter: t})
    }), $(".portfolio-categories").on("click", "li", function () {
        var t = $(this).attr("data-filter");
        $("#equal-three-column").isotope({filter: t})
    }), $(".portfolio-categories li a").on("click", function (t) {
        $(".portfolio-categories li a").removeClass("active"), $(this).addClass("active")
    }), smoothScroll.init({
        offset: 90,
        speed: 1e3,
        updateURL: !1
    }), jQuery("body").attr("data-spy", "scroll").attr("data-target", "#nav-wrapper").attr("data-offset", "100"), jQuery("a.scrollto").on("click", function () {
        event.preventDefault(), jQuery(".navbar-collapse").hasClass("in") && jQuery(".navbar-collapse").removeClass("in").addClass("collapse")
    }), $(document).on("click", ".language_bar li", function () {
        var t = $(this).index();
        0 == t && $(".glob").removeClass("one").removeClass("two"), 1 == t && $(".glob").addClass("one").removeClass("two"), 2 == t && $(".glob").addClass("two").removeClass("one")
    }), MasonryPortfolio()
}), $(".selectpicker").selectpicker(), $(window).on("resize", function () {
    jQuery('[data-spy="scroll"]').each(function () {
        jQuery(this).scrollspy("refresh")
    })
}), $(".play-button").on("click", function () {
    $("video").prop("autoplay", !0), $(".play-button").toggleClass("off")
});
var isClicked = !1;

function MasonryPortfolio() {
    if ($("#portfolio-wrap").length > 0) {
        var t = $("#portfolio");
        t.isotope({
            itemSelector: ".grid-item",
            gutter: 0,
            transitionDuration: "0.5s"
        }), $("#filters a").on("click", function () {
            $("#filters a").removeClass("active"), $(this).addClass("active");
            var e = $(this).attr("data-filter");
            return t.isotope({filter: e}), !1
        }), $(document).scroll(function () {
            $(".auto-construct").length > 0 && ($(this).scrollTop() > $("#portfolio").offset().top + $("#portfolio").height() - window.innerHeight && $("#portfolio").removeClass("auto-construct"))
        }), $(window).on("resize", function () {
            var e = window.innerWidth;
            s = 1;
            var i = $("#portfolio").attr("data-col");
            if (e >= 1466) {
                $("#portfolio-wrap").css({width: e - 20 + "px"}), $("#portfolio-wrap.no-gutter").css({width: e});
                var o, a = $("#portfolio-wrap").width();
                s = void 0 !== i && !1 !== i ? $("#portfolio").attr("data-col") : 3, o = Math.floor(a / s), t.find(".grid-item").each(function () {
                    $(".grid-item").css({
                        width: o - 20 + "px",
                        height: .948 * o - 20 + "px",
                        margin: "10px"
                    }), $(".no-gutter .grid-item").css({
                        width: o + "px",
                        height: .908 * o + "px",
                        margin: "0px"
                    }), $(".grid-item.wide").css({width: 2 * o - 20 + "px"}), $(".no-gutter .grid-item.wide").css({width: 2 * o + "px"}), $(".grid-item.tall").css({height: 1.875 * o - 20 + "px"}), $(".no-gutter .grid-item.tall").css({height: 1.82 * o + "px"}), $(".grid-item.wide-tall").css({
                        width: 2 * o - 25 + "px",
                        height: 2 * o - 60 + "px"
                    }), $(".no-gutter .grid-item.wide-tall").css({
                        width: 2 * o + "px",
                        height: 1.5 * o + "px"
                    }), $(".grid-item.small").css({height: 2 * o - 60 + "px"}), $(".no-gutter .grid-item.small").css({height: .588 * o + "px"})
                })
            } else if (e >= 1024) {
                $("#portfolio-wrap").css({width: e - 20 + "px"}), $("#portfolio-wrap.no-gutter").css({width: e});
                a = $("#portfolio-wrap").width();
                s = void 0 !== i && !1 !== i ? $("#portfolio").attr("data-col") : 3, o = Math.floor(a / s), t.find(".grid-item").each(function () {
                    $(".grid-item").css({
                        width: o - 20 + "px",
                        height: .948 * o - 20 + "px",
                        margin: "10px"
                    }), $(".no-gutter .grid-item").css({
                        width: o + "px",
                        height: .9089 * o + "px",
                        margin: "0px"
                    }), $(".grid-item.wide").css({width: 2 * o - 20 + "px"}), $(".no-gutter .grid-item.wide").css({width: 2 * o + "px"}), $(".grid-item.tall").css({height: 1.875 * o - 20 + "px"}), $(".no-gutter .grid-item.tall").css({height: 1.82 * o + "px"}), $(".grid-item.wide-tall").css({
                        width: 2 * o - 25 + "px",
                        height: 2 * o - 60 + "px"
                    }), $(".no-gutter .grid-item.wide-tall").css({width: 2 * o + "px", height: 1.815 * o + "px"})
                })
            } else if (e >= 768) {
                $("#portfolio-wrap").css({width: e - 20 + "px"}), $("#portfolio-wrap.no-gutter").css({width: e});
                a = $("#portfolio-wrap").width();
                s = void 0 !== i && !1 !== i ? $("#portfolio").attr("data-col") : 3, o = Math.floor(a / s), t.find(".grid-item").each(function () {
                    $(".grid-item").css({
                        width: o - 20 + "px",
                        height: .948 * o - 20 + "px",
                        margin: "10px"
                    }), $(".no-gutter .grid-item").css({
                        width: o + "px",
                        height: .9089 * o + "px",
                        margin: "0px"
                    }), $(".grid-item.wide").css({width: 2 * o - 20 + "px"}), $(".no-gutter .grid-item.wide").css({width: 2 * o + "px"}), $(".grid-item.tall").css({height: 1.875 * o - 20 + "px"}), $(".no-gutter .grid-item.tall").css({height: 1.82 * o + "px"}), $(".grid-item.wide-tall").css({
                        width: 2 * o - 24 + "px",
                        height: 2 * o - 50 + "px"
                    }), $(".no-gutter .grid-item.wide-tall").css({width: 2 * o + "px", height: 1.815 * o + "px"})
                })
            } else if (e < 767 && e > 481) {
                $("#portfolio-wrap").css({width: e - 20 + "px"}), $("#portfolio-wrap.no-gutter").css({width: e});
                a = $("#portfolio-wrap").width();
                var s = 2;
                o = Math.floor(a / s), t.find(".grid-item").each(function () {
                    $(".grid-item").css({
                        width: o - 20 + "px",
                        height: .948 * o - 20 + "px",
                        margin: "10px"
                    }), $(".no-gutter .grid-item").css({
                        width: o + "px",
                        height: .75 * o + "px",
                        margin: "0px"
                    }), $(".grid-item.wide").css({width: 2 * o - 20 + "px"}), $(".no-gutter .grid-item.wide").css({width: 2 * o + "px"}), $(".grid-item.tall").css({height: 1.5 * o - 20 + "px"}), $(".no-gutter .grid-item.tall").css({height: 1.5 * o + "px"}), $(".grid-item.wide-tall").css({
                        width: 2 * o - 20 + "px",
                        height: 1.5 * o - 20 + "px"
                    }), $(".no-gutter .grid-item.wide-tall").css({width: 2 * o + "px", height: 1.5 * o + "px"})
                })
            } else if (e <= 480) {
                $("#portfolio-wrap").css({width: e - 20 + "px"}), $("#portfolio-wrap.no-gutter").css({width: e});
                a = $("#portfolio-wrap").width(), s = 1;
                o = Math.floor(a / s), t.find(".grid-item").each(function () {
                    $(".grid-item").css({
                        width: o + "px",
                        height: .75 * o + "px",
                        margin: "10px0px"
                    }), $(".no-gutter .grid-item").css({
                        width: o + "px",
                        height: .75 * o + "px",
                        margin: "0px"
                    }), $(".grid-item.wide").css({width: o + "px"}), $(".no-gutter .grid-item.wide").css({width: o + "px"}), $(".grid-item.tall").css({height: 1.5 * o + "px"}), $(".no-gutter .grid-item.tall").css({height: 1.5 * o + "px"}), $(".grid-item.wide-tall").css({
                        width: o + "px",
                        height: .75 * o + "px"
                    }), $(".no-gutter .grid-item.wide-tall").css({width: o + "px", height: .75 * o + "px"})
                })
            }
            return s
        }).resize(), $("#all").click()
    }
}

jQuery(".panel-heading a").on("click", function () {
    $(".panel-heading").removeClass("actives"), $(this).parents(".panel-heading").addClass("actives"), isClicked = !0
}), $(".panel-heading a").on("click", function () {
    isClicked && $(this).parents(".panel-heading").toggleClass("act")
}), jQuery(window).on("scroll", function () {
    var t, e;
    t = jQuery(document).scrollTop(), e = jQuery("#home-section").height() + 450, jQuery("#home-section").css({opacity: 1 - t / e * 1.2}), t >= 10 ? jQuery("#nav-wrapper").addClass("menubgC") : jQuery("#nav-wrapper").removeClass("menubgC");
    var i = $(window).scrollTop();
    $(".case-intro").length > 0 && (i >= $(".case-intro").offset().top - 105 ? $(".float-navigation").css("position", "fixed").css("top", "100px") : $(".float-navigation").css("position", "relative").css("top", "0px"));
    $(".case-challenge").length > 0 && (i >= $(".case-challenge").offset().top - 150 ? $(".chall").addClass("active") : $(".chall").removeClass("active"));
    $(".case-approach").length > 0 && (i >= $(".case-approach").offset().top - 150 ? ($(".appr").addClass("active"), $(".chall").removeClass("active")) : $(".appr").removeClass("active"));
    $(".case-benefits").length > 0 && (i >= $(".case-benefits").offset().top - 150 ? ($(".ben").addClass("active"), $(".appr").removeClass("active")) : $(".ben").removeClass("active"));
    $(".studies-about").length > 0 && (i >= $(".studies-about").offset().top - 510 && $(".float-navigation").css("position", "absolute").css("top", "-400px"))
}), $(".clients2").owlCarousel({
    loop: !0,
    autoplay: !0,
    nav: !0,
    dots: !1,
    smartSpeed: 500,
    margin: 0,
    responsiveClass: !0,
    responsive: {0: {items: 1}, 480: {items: 2}, 1280: {items: 4}, 1024: {items: 3}, 1550: {items: 5}}
}), $(".clients3").owlCarousel({
    loop: !1,
    autoplay: !1,
    nav: !0,
    dots: !1,
    smartSpeed: 1500,
    margin: 0,
    responsiveClass: !0,
    responsive: {0: {items: 1}, 480: {items: 2}, 1280: {items: 3}, 1024: {items: 2}, 1550: {items: 4}}
}), $(".testimonials").owlCarousel({
    loop: !0,
    autoplay: !0,
    nav: !0,
    dots: !1,
    smartSpeed: 1500,
    margin: 0,
    responsiveClass: !0,
    responsive: {0: {items: 1}, 480: {items: 1}, 1e3: {items: 1}}
}), $(".team").owlCarousel({
    loop: !1,
    autoplay: !1,
    nav: !1,
    dots: !0,
    smartSpeed: 1500,
    margin: 0,
    responsiveClass: !0,
    responsive: {0: {items: 1}, 480: {items: 1}, 1e3: {items: 1}}
}), $("body").hasClass("journey") && $(".index_4") && ($("#parallaxBlock1").parallax("-10%", -.1), $("#parallaxBlock2").parallax("10%", .1), $("#parallaxBlock3").parallax("-10%", -.1), $("#parallaxBlock4").parallax("10%", .1)), $("a[data-gal^='prettyPhoto']").prettyPhoto(), $("#portfolio:first a[data-gal^='prettyPhoto']").prettyPhoto({
    animation_speed: "normal",
    theme: "light_square",
    slideshow: 3e3,
    autoplay_slideshow: !1
}), $("#portfolio:gt(0) a[data-gal^='prettyPhoto']").prettyPhoto({
    animation_speed: "fast",
    slideshow: 1e4,
    hideflash: !0
}), $(".video-popup-block .slide-title").magnificPopup({
    delegate: "a",
    type: "iframe",
    tLoading: "Loading image #%curr%...",
    mainClass: "mfp-fade",
    removalDelay: 160,
    preloader: !1,
    fixedContentPos: !1,
    gallery: {enabled: !0, navigateByImgClick: !0, preload: [0, 1]},
    image: {tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'}
});
